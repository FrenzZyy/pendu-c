#include "pendu.h"
#include "joueur.h"

bool Pendu::DansMot(char c) const
{
    return (m_aDeviner.find(c) != string::npos) and (m_tentative.find(c) == string::npos);
}

void Pendu::inclureDansMot(char c)
{
    for (unsigned j = 0; j< m_tentative.size(); ++j)
        if (c == m_aDeviner[j])
            m_tentative[j] = c;
}

//permet au joueur de choisir le mot a deviner
void Pendu::initADeviner(unsigned p)
{
    cout << "Au " << m_joueurs[p]->getNom() << " de proposer un mot" << endl;
    m_aDeviner = m_joueurs[p]->proposerMot();
}

void Pendu::initTentative()
{
    m_tentative = string(m_aDeviner.size(), '_');
}

//permet au joueurs de proposer une lettre a tour de rôle, return vrai si le mot est deviner sinon return faux
bool Pendu::devinerMot(unsigned p)
{
    //tant qu'un joueur trouve une lettre et que le mot n'est pas deviner on continue a jouer
    while (true) {
        for (unsigned j = 0; j < m_joueurs.size(); ++j) {
            //le joueur p propose le mot donc on passe son tour
            if (j == p) {
                continue;
            }
            //reference sur le joueur j
            Joueur &joueur = *m_joueurs[j];

            start:
            //affiche la derniere tentative, le status du joueur j, et demande sa lettre proposer
            cout << "Mot a deviner : " << m_tentative << endl;
            cout << "Au " << joueur.getNom() << " de choisir une lettre, ";
            char lettre = joueur.proposerLettre();
            //actualise le score du joueur
            if (DansMot(lettre)) {
                inclureDansMot(lettre);
                cout << "mot restant : " << m_tentative << endl;
                if (m_tentative == m_aDeviner) {
                    joueur.incrPoints();
                    cout << " ==> GAGNE !" << endl;
                    return true;
                }
            }
            else {
                joueur.incrEtape();
                cout << "Pas la bonne lettre, recommence " << endl;
                goto start;
            }
        }
    }
    return true;
}

Pendu::Pendu(const vector<Joueur*>& j)
    :m_joueurs(j)
{
}

void Pendu::toString() const
{
    cout<<"la partie est terminer. voici les resultats :"<<endl;
    for (unsigned j = 0; j < m_joueurs.size();++j) {
        Joueur &joueur = *m_joueurs[j];
        cout << "Joueur " << joueur.getNom() << " (" << (j + 1) << ") ==> " << " (points: " << joueur.getPoints() << ")" << endl;
    }
    cout<<endl;
}

void Pendu::play()
{
    //reinitialise les status des joueurs
    for (unsigned p = 0; p < m_joueurs.size(); p++)
        m_joueurs[p]->reset();

    // Fait jouer chacun des joueurs a tour de roles
    for (unsigned p = 0; p < m_joueurs.size(); p++) {
        //initialise le mot a deviner par le joueur p
        initADeviner(p);
        // initialise la tentative
        initTentative();
        // les joueurs autres que p sont solliciter pour proposer des lettres
        bool b = devinerMot(p);
        if (!b)
            cout << "Le mot a deviner etait : " << m_aDeviner << endl;
    }
}



