#include <iostream>
#include <vector>
#include "pendu.h"
#include "joueur.h"
using namespace std;

int main()
{
    // Instancie les joueurs
    Joueur j1("Joueur 1");
    Joueur j2("Joueur 2");
    vector<Joueur*> joueurs;
    joueurs.push_back(&j1);
    joueurs.push_back(&j2);
    // Instancie un jeu du Pendu
    Pendu jeu(joueurs);
    // Effectue les parties
    char c = 'o';
    while (tolower(c) == 'o')
    {
    jeu.play();
    jeu.toString();
#ifdef __unix__
system("clear");
#elif defined(_WIN32) || defined(WIN32)
   system("cls");
#endif
    cout<<"Une autre partie (o/n)? ";
    cin>>c;
#ifdef __unix__
system("clear");
#elif defined(_WIN32) || defined(WIN32)
   system("cls");
#endif
   }
}
