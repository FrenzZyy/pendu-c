#ifndef JOUEUR_H
#define JOUEUR_H

#include <iostream>
#include <string>

using namespace std;

class Joueur
{
private:
    string m_nom;
    int m_npoints;
    int m_etape;

public:
    Joueur(const string& n);
    virtual ~Joueur();
    const string& getNom() const;
    int getPoints() const;
    int getEtape() const;
    void incrPoints();
    void incrEtape();
    void reset();
    string proposerMot() const;
    char proposerLettre() const;

};

#endif // JOUEUR_H
