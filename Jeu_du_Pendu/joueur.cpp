#include "joueur.h"

Joueur::Joueur(const string& n)
    :m_nom(n), m_npoints(0), m_etape(0)
{
}

Joueur::~Joueur()
{
}

const string& Joueur::getNom() const
{
    return m_nom;
}

int Joueur::getPoints() const
{
    return m_npoints;
}

int Joueur::getEtape() const
{
    return m_etape;
}

void Joueur::incrPoints()
{
    ++m_npoints;
}

void Joueur::incrEtape()
{
    ++m_etape;
}

void Joueur::reset()
{
    m_etape = 0;
}

string Joueur::proposerMot() const
{
    cout<<"entrer le mot :"<<endl;
    string mot;
    cin>>mot;
    return mot;
}

char Joueur::proposerLettre() const
{
    cout<<"entrer la lettre :"<<endl;
    string lettre;
    cin>>lettre;
    return lettre[0];
}
