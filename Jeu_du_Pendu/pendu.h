#ifndef PENDU_H
#define PENDU_H

#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>
#include "joueur.h"

using namespace std;

class Pendu
{
private:
    const vector<Joueur*> &m_joueurs;
    string m_aDeviner;
    string m_tentative;

protected:
    bool DansMot(char c) const;
    void inclureDansMot(char c);
    void initADeviner(unsigned p);
    void initTentative();
    bool devinerMot(unsigned p);

public:
    Pendu(const vector<Joueur*>& j);
    void toString() const;
    void play();
};

#endif // PENDU_H
